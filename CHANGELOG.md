# Changelog

All notable changes to this project will be documented in this file.

## [Unreleased]

### Added

- Initial project
- Add Docker w/ Node and docker-compose w/ MongoDB

## Release [v.X.X.X] - YYYY-MM-DD

### Added

### Changed

### Deprecated

### Fixed

### Removed

### Security
